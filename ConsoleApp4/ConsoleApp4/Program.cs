﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        public static char[] separators = new char[] { ' ', ',', '-', '.', '!', '?', '\"', '\n', '\r', '"' };

        public static Stopwatch sw;
        public static Dictionary<string, int> dicWordRarity;
        public static Dictionary<int, int> dicWordLength;

        public static void Task1()
        {
            dicWordRarity = new Dictionary<string, int>();
            dicWordLength = new Dictionary<int, int>();

            sw = new Stopwatch();

            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\..\..", "*.txt");

            sw.Start();
            foreach (var i in files)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(i))
                    {
                        string line;
                        
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] words = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                            for (int j = 0; j < words.Length; j++)
                            {
                                string word = words[j].ToLower();
                                if (dicWordRarity.ContainsKey(word))
                                    dicWordRarity[word]++;
                                else
                                    dicWordRarity.Add(word, 1);

                                if (dicWordLength.ContainsKey(word.Length))
                                    dicWordLength[word.Length]++;
                                else
                                    dicWordLength.Add(word.Length, 1);
                            }
                        }
                        
                    }
                }
                catch (IOException)
                {
                    Console.WriteLine("No file.");
                }
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            Console.WriteLine("Общая статистика по встречаемости слов /// Последовательный алгоритм");

            Console.WriteLine("1) Число уникальных слов:\n" + dicWordRarity.Count);

            Console.Write("2) 10 самых часто встречающихся слов: \n[ ");
            foreach(var i in dicWordRarity.OrderByDescending(x => x.Value).Take(10))
            {
                Console.Write(i.Key + " ");
            }
            Console.Write("]\n");

            Console.WriteLine("3) Распределения числа слов по длине:");
            Console.WriteLine("Кол. букв | Кол. слов");
            foreach(var i in dicWordLength.OrderByDescending(x => x.Value))
            {
                Console.WriteLine("    " + i.Key.ToString() + "\t\t" + i.Value.ToString());
            }
                        
            Console.WriteLine("\nВремя выполнения: " + ts.TotalMilliseconds + " ms\n");
        }

        public static void Task2()
        {
            dicWordRarity = new Dictionary<string, int>();
            dicWordLength = new Dictionary<int, int>();

            sw = new Stopwatch();

            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\..\..", "*.txt");

            sw.Start();
            try
            {
                dicWordRarity = files.SelectMany(path => File.ReadLines(path))
                .SelectMany(line => line.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                .GroupBy(word => word.ToLower())
                .ToDictionary(data => data.Key.ToLower(), data => data.Count());

                dicWordLength = files.SelectMany(path => File.ReadLines(path))
                .SelectMany(line => line.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                .GroupBy(word => word.ToLower().Length)
                .ToDictionary(data => data.Key, data => data.Count());
            }
            catch (IOException)
            {
                Console.WriteLine("No file.");
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            Console.WriteLine("Общая статистика по встречаемости слов /// Алгоритм с применением LINQ запросов");

            Console.WriteLine("1) Число уникальных слов:\n" + dicWordRarity.Count);

            Console.Write("2) 10 самых часто встречающихся слов: \n[ ");
            foreach (var i in dicWordRarity.OrderByDescending(x => x.Value).Take(10))
            {
                Console.Write(i.Key + " ");
            }
            Console.Write("]\n");

            Console.WriteLine("3) Распределения числа слов по длине:");
            Console.WriteLine("Кол. букв | Кол. слов");
            foreach (var i in dicWordLength.OrderByDescending(x => x.Value))
            {
                Console.WriteLine("    " + i.Key.ToString() + "\t\t" + i.Value.ToString());
            }

            Console.WriteLine("\nВремя выполнения: " + ts.TotalMilliseconds + " ms\n");
        }

        public static void Task3()
        {
            ConcurrentDictionary<string, int> dicWordRarity_conc = new ConcurrentDictionary<string, int>();
            ConcurrentDictionary<int, int> dicWordLength_conc = new ConcurrentDictionary<int, int>();

            sw = new Stopwatch();

            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\..\..", "*.txt");

            sw.Start();
            try 
            { 
                Parallel.ForEach(files.SelectMany(path => File.ReadLines(path)), line =>
                {
                    string[] words = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < words.Length; j++)
                    {
                        string word = words[j].ToLower();
                        dicWordRarity_conc.AddOrUpdate(word, 1, (key, value) => value + 1);
                        dicWordLength_conc.AddOrUpdate(word.Length, 1, (key, value) => value + 1);
                    }
                });
            }
            catch (IOException)
            {
                Console.WriteLine("No file.");
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            Console.WriteLine("Общая статистика по встречаемости слов /// Алгоритм на базе методов Parallel.For или Parallel.ForEach");

            Console.WriteLine("1) Число уникальных слов:\n" + dicWordRarity.Count);

            Console.Write("2) 10 самых часто встречающихся слов: \n[ ");
            foreach (var i in dicWordRarity_conc.OrderByDescending(x => x.Value).Take(10))
            {
                Console.Write(i.Key + " ");
            }
            Console.Write("]\n");

            Console.WriteLine("3) Распределения числа слов по длине:");
            Console.WriteLine("Кол. букв | Кол. слов");
            foreach (var i in dicWordLength_conc.OrderByDescending(x => x.Value))
            {
                Console.WriteLine("    " + i.Key.ToString() + "\t\t" + i.Value.ToString());
            }

            Console.WriteLine("\nВремя выполнения: " + ts.TotalMilliseconds + " ms\n");
        }

        public static void Task4()
        {
            dicWordRarity = new Dictionary<string, int>();
            dicWordLength = new Dictionary<int, int>();

            sw = new Stopwatch();

            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\..\..", "*.txt");

            sw.Start();
            try
            {
                dicWordRarity = files.AsParallel()
                .SelectMany(path => File.ReadLines(path))
                .SelectMany(line => line.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                .GroupBy(word => word.ToLower())
                .ToDictionary(data => data.Key.ToLower(), data => data.Count());

                dicWordLength = files.AsParallel()
                .SelectMany(path => File.ReadLines(path))
                .SelectMany(line => line.Split(separators, StringSplitOptions.RemoveEmptyEntries))
                .GroupBy(word => word.ToLower().Length)
                .ToDictionary(data => data.Key, data => data.Count());
            }
            catch (IOException)
            {
                Console.WriteLine("No file.");
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;

            Console.WriteLine("Общая статистика по встречаемости слов /// Алгоритм с применением PLINQ запросов");

            Console.WriteLine("1) Число уникальных слов:\n" + dicWordRarity.Count);

            Console.Write("2) 10 самых часто встречающихся слов: \n[ ");
            foreach (var i in dicWordRarity.OrderByDescending(x => x.Value).Take(10))
            {
                Console.Write(i.Key + " ");
            }
            Console.Write("]\n");

            Console.WriteLine("3) Распределения числа слов по длине:");
            Console.WriteLine("Кол. букв | Кол. слов");
            foreach (var i in dicWordLength.OrderByDescending(x => x.Value))
            {
                Console.WriteLine("    " + i.Key.ToString() + "\t\t" + i.Value.ToString());
            }

            Console.WriteLine("\nВремя выполнения: " + ts.TotalMilliseconds + " ms\n");
        }

        static void Main(string[] args)
        {
            Task1();
            Task2();
            Task3();
            Task4();
        }
    }
}
