﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace ConsoleApp1
{
    struct Segment
    {
        public int Beg { get; set; }
        public int End { get; set; }
    }

    struct Gap
    {
        public int Start { get; set; }
        public int Finish { get; set; }
        public int Step { get; set; }
    }

    class Program
    {
        /*
        const int N = 10;
        const int M = 1;

        static double[] a;

        static DateTime dt1;

        // 1.
        static void Posled(double[] arr)
        {
            for(int i = 0; i < N; ++i)
            {
                arr[i] = Math.Pow(arr[i], 1.789);
            }
        }

        // 2.          
        static void Run(object some_data)
        {
            double[] arr = (double[])some_data;

            for(int i = 0; i < N; ++i)
            {
                arr[i] = Math.Pow(arr[i], 2.789);
            }
        }

        static void Run1(object some_data)
        {
            double[] arr = (double[])some_data;

            for (int i = 0; i < N; i += M)
            {
                arr[i] = Math.Pow(arr[i], 3.789);
            }
        }

        static void Run2(object some_data)
        {
            double[] arr = (double[])some_data;

            for (int i = 1; i < N; i += M)
            {
                arr[i] = Math.Pow(arr[i], 3.789);
            }
        }
        */

        static double[] src;
        static double[] dst;
        private const int _N = 100000;
        private const int _M = 2;
        private const int _K = 5;

        static string path = Path.GetFullPath(@"..\..\..\");
        static List<string> text = new List<string>();
        static Stopwatch sw = new Stopwatch();
        static Stopwatch swL = new Stopwatch();
        static TimeSpan ts;
        static TimeSpan tsL;

        static Segment a1 = new Segment();
        static Segment a2 = new Segment();
        static Segment b = new Segment();
        static Gap g = new Gap();

        static int[] N = { 10, 100, 1000, 100000 };
        static int[] M = { 2, 3, 4, 5, 10 };
        static Thread[] threads;

        static void Init()
        {
            for (int i = 0; i < src.Length; i++)
            {
                src[i] = i + 1;
            }
        }

        static void RunSimple(object o)
        {
            Segment b = (Segment)o;

            for (int i = b.Beg; i < b.End; i++)
            {
                dst[i] = Math.Pow(src[i], 1.789);
            }
        }

        static void RunComplex(object o)
        {
            Segment b = (Segment)o;

            for (int i = b.Beg; i < b.End; i++)
            {
                for (int j = 0; j < _K; j++)
                {
                    dst[i] += Math.Pow(src[i], 1.789);
                }
            }
        }

        static void RunUnbalanced(object o)
        {
            Segment b = (Segment)o;

            for (int i = b.Beg; i < b.End; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    dst[i] += Math.Pow(src[i], 1.789);
                }
            }
        }

        static void RunCircled(object o)
        {
            Gap g = (Gap)o;

            for (int i = g.Start; i < g.Finish; i += g.Step)
            {
                dst[i] += Math.Pow(src[i], 1.789);
            }
        }

        static void Task_1()
        {
            text.Add("Последовательная обработка элементов вектора:");
            Console.WriteLine(text[text.Count - 1]);
            src = new double[_N];
            dst = new double[_N];

            Init();

            sw.Start();
            for (int i = 0; i < _N; i++)
            {
                dst[i] = Math.Pow(src[i], 1.789);
            }
            sw.Stop();
            ts = sw.Elapsed;
            text.Add("N = " + _N + " : Time elapsed " + ts.TotalMilliseconds + " ms");
            Console.WriteLine(text[text.Count - 1]);
        }

        static void Task_2()
        {
            text.Add("\nМногопоточная обработка элементов вектора:");
            Console.WriteLine(text[text.Count - 1]);
            
            threads = new Thread[_M] 
            { 
                new Thread(RunSimple), 
                new Thread(RunSimple) 
            };

            Init();

            sw = new Stopwatch();

            int split = src.Length / _M;

            a1.Beg = 0;
            a1.End = a1.Beg + split;

            a2.Beg = a1.End;
            a2.End = a2.Beg + split;

            sw.Start();
            threads[0].Start(a1);
            threads[1].Start(a2);

            threads[0].Join();
            threads[1].Join();
            sw.Stop();

            ts = sw.Elapsed;
            text.Add("N = " + _N + " : M = " + _M + " : Time elapsed " + ts.TotalMilliseconds + " ms");
            Console.WriteLine(text[text.Count - 1]);
        }

        static void Task_3()
        {
            text.Add("\nАнализ эффективности многопоточной обработки при разных параметрах: ");
            Console.WriteLine(text[text.Count - 1]);
            for (int i = 0; i < N.Length; i++)
            {
                src = new double[N[i]];
                dst = new double[N[i]];

                for (int j = 0; j < M.Length; j++)
                {
                    Init();

                    threads = new Thread[M[j]];
                    sw = new Stopwatch();
                    swL = new Stopwatch();

                    // Замер без многопоточности
                    swL.Start();
                    for (int t = 0; t < src.Length; t++)
                    {
                        dst[t] = Math.Pow(src[t], 1.789);
                    }
                    swL.Stop();

                    tsL = swL.Elapsed;

                    // Замер с многопоточностью
                    Init();

                    for (int t = 0; t < threads.Length; t++)
                    {
                        threads[t] = new Thread(RunSimple);
                    }                    

                    sw.Start();
                    for (int k = 0; k < threads.Length; k++)
                    {
                        b.Beg = k * (src.Length / M[j]);
                        b.End = b.Beg + (src.Length / M[j]);

                        threads[k].Start(b);
                    }

                    for (int l = 0; l < threads.Length; l++)
                    {
                        threads[l].Join();
                    }
                    sw.Stop();

                    ts = sw.Elapsed;
                    text.Add("N = " + N[i] + "  \t | M = " + M[j] + " \t | Time elapsed " + ts.TotalMilliseconds + " ms\t | Non-parallel " + tsL.TotalMilliseconds);
                    Console.WriteLine(text[text.Count - 1]);
                }
            }
        }

        static void Task_4()
        {
            text.Add("\nAнализ эффективности при усложнении обработки каждого элемента: ");
            Console.WriteLine(text[text.Count - 1]);
            for (int i = 0; i < N.Length; i++)
            {
                src = new double[N[i]];
                dst = new double[N[i]];

                for (int j = 0; j < M.Length; j++)
                {
                    Init();

                    threads = new Thread[M[j]];
                    sw = new Stopwatch();
                    swL = new Stopwatch();

                    // Замер без многопоточности
                    swL.Start();
                    for (int t = 0; t < src.Length; t++)
                    {
                        for (int s = 0; s < _K; s++)
                        {
                            dst[t] += Math.Pow(src[t], 1.789);
                        }
                    }
                    swL.Stop();

                    tsL = swL.Elapsed;

                    // Замер с многопоточностью
                    Init();

                    for (int t = 0; t < threads.Length; t++)
                    {
                        threads[t] = new Thread(RunComplex);
                    }

                    sw.Start();
                    for (int k = 0; k < threads.Length; k++)
                    {
                        b.Beg = k * (src.Length / M[j]);
                        b.End = b.Beg + (src.Length / M[j]);

                        threads[k].Start(b);
                    }

                    for (int l = 0; l < threads.Length; l++)
                    {
                        threads[l].Join();
                    }
                    sw.Stop();

                    ts = sw.Elapsed;
                    text.Add("N = " + N[i] + "  \t | M = " + M[j] + " \t | Time elapsed " + ts.TotalMilliseconds + " ms\t | Non-parallel " + tsL.TotalMilliseconds);
                    Console.WriteLine(text[text.Count - 1]);
                }
            }
        }

        static void Task_5()
        {
            text.Add("\nЭффективность разделения по диапазону при неравномерной вычислительной сложности: ");
            Console.WriteLine(text[text.Count - 1]);
            // для 100000 работает кошмарно долго
            for (int i = 0; i < N.Length - 1; i++)
            {
                src = new double[N[i]];
                dst = new double[N[i]];

                for (int j = 0; j < M.Length; j++)
                {
                    Init();

                    threads = new Thread[M[j]];
                    sw = new Stopwatch();
                    swL = new Stopwatch();

                    // Замер без многопоточности
                    swL.Start();
                    for (int t = 0; t < src.Length; t++)
                    {
                        for (int s = 0; s < t; s++)
                        {
                            dst[t] += Math.Pow(src[t], 1.789);
                        }
                    }
                    swL.Stop();

                    tsL = swL.Elapsed;

                    Init();

                    // Замер с многопоточностью
                    for (int t = 0; t < threads.Length; t++)
                    {
                        threads[t] = new Thread(RunUnbalanced);
                    }

                    sw.Start();
                    for (int k = 0; k < threads.Length; k++)
                    {
                        b.Beg = k * (src.Length / M[j]);
                        b.End = b.Beg + (src.Length / M[j]);

                        threads[k].Start(b);
                    }

                    for (int l = 0; l < threads.Length; l++)
                    {
                        threads[l].Join();
                    }
                    sw.Stop();

                    ts = sw.Elapsed;
                    text.Add("N = " + N[i] + "  \t | M = " + M[j] + " \t | Time elapsed " + ts.TotalMilliseconds + " ms \t | Non-parallel " + tsL.TotalMilliseconds);
                    Console.WriteLine(text[text.Count - 1]);
                }
            }
        }

        static void Task_6()
        {
            text.Add("\nЭффективность параллелизма при круговом разделении элементов вектора: ");
            Console.WriteLine(text[text.Count - 1]);

            for (int i = 0; i < N.Length; i++)
            {
                src = new double[N[i]];
                dst = new double[N[i]];

                for (int j = 0; j < M.Length; j++)
                {
                    Init();

                    threads = new Thread[M[j]];
                    sw = new Stopwatch();

                    for (int t = 0; t < threads.Length; t++)
                    {
                        threads[t] = new Thread(RunCircled);
                    }

                    sw.Start();
                    for (int k = 0; k < threads.Length; k++)
                    {
                        g.Start = k;
                        g.Finish = src.Length;
                        g.Step = threads.Length;

                        threads[k].Start(g);
                    }

                    for (int l = 0; l < threads.Length; l++)
                    {
                        threads[l].Join();
                    }
                    sw.Stop();

                    ts = sw.Elapsed;
                    text.Add("N = " + N[i] + "  \t | M = " + M[j] + " \t | Time elapsed " + ts.TotalMilliseconds + " ms");
                    Console.WriteLine(text[text.Count - 1]);
                }
            }
        }

        static void Main(string[] args)
        {
            //a = new double[N];
            //for (int i = 0; i < N; ++i)
            //{
            //    a[i] = i + 1;
            //}
            //Console.WriteLine("Usual: done!");
            //dt1 = DateTime.Now;
            //Posled(a);
            //Console.WriteLine("Time elapsed: {0}\n", (DateTime.Now - dt1).TotalMilliseconds);
            //Thread thr = new Thread(Run);
            //dt1 = DateTime.Now;
            //thr.Start(a);
            //thr.Join();
            //Console.WriteLine("One thread: done!");
            //Console.WriteLine("Time elapsed: {0}\n", (DateTime.Now - dt1).TotalMilliseconds);
            //Thread thr1 = new Thread(Run1);
            //Thread thr2 = new Thread(Run2);
            //dt1 = DateTime.Now;
            //thr1.Start(a);
            //thr2.Start(a);
            //thr1.Join();
            //thr2.Join();
            //Console.WriteLine("Two threads: done!");
            //Console.WriteLine("Time elapsed: {0}\n", (DateTime.Now - dt1).TotalMilliseconds);
            //// РЕализовать массив потоков и в функцию передавать индекс диапозона
            //Thread[] threads = new Thread[2];
            //threads

            text.Add("Колличество процессоров: " + System.Environment.ProcessorCount + "\n");
            Console.WriteLine(text[text.Count - 1]);

            // 1.
            Task_1();

            // 2.
            Task_2();

            // 3.
            Task_3();

            // 4.
            Task_4();

            // 5.
            Task_5();

            // 6.
            Task_6();

            // Save records
            using (StreamWriter output = new StreamWriter(Path.Combine(path, "results.txt")))
            {
                foreach (string line in text) output.WriteLine(line);
            }
        }       
    }
}
