﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Diagnostics;

namespace ConsoleApp3
{
    class Program
    {
        static int N = 100000;
        static int group = 2;

        static string buffer;
        static List<List<string>> check_reader;
        static List<List<string>> check_writer;

        static bool finish;
        static bool bEmpty;

        static Thread[] Readers;
        static Thread[] Writers;

        static Stopwatch sw;

        static object obj;

        static Semaphore read_semaphore;
        static Semaphore write_semaphore;

        static int can_read;
        static int can_write;

        static void Read()
        {
            List<string> temp = new List<string>();

            while (!finish)
            {
                if (!bEmpty)
                {
                    temp.Add(buffer);
                    bEmpty = true;
                }
            }

            check_reader.Add(temp);

            Console.WriteLine("Reader " + Thread.CurrentThread.ManagedThreadId + " reads " + temp.Count + " records.");
        }
        static void Write()
        {
            List<string> temp = Enumerable.Range(0, N).Select(
                n => n.ToString() + " " + Thread.CurrentThread.ManagedThreadId
                ).ToList();

            int i = 0;
            while (i < N)
            {
                if (bEmpty)
                {
                    buffer = temp[i++];
                    bEmpty = false;
                }
            }

            check_writer.Add(temp);

            Console.WriteLine("Writer " + Thread.CurrentThread.ManagedThreadId + " wrote " + i + " records.");
        }
        static void Task1()
        {
            Readers = new Thread[group];
            Writers = new Thread[group];

            sw = new Stopwatch();

            finish = false;
            bEmpty = true;


            sw.Start();
            check_reader = new List<List<string>>();
            check_writer = new List<List<string>>();

            for (int i = 0; i < group; i++)
            {
                Writers[i] = new Thread(Write);
                Writers[i].Start();
            }
            for (int i = 0; i < group; i++)
            {
                Readers[i] = new Thread(Read);
                Readers[i].Start();
            }

            for (int i = 0; i < group; i++)
            {
                Writers[i].Join();
            }

            finish = true;

            for (int i = 0; i < group; i++)
            {
                Readers[i].Join();
            }
            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            Console.WriteLine("Task 1:\tNon-synced: " + ts.TotalMilliseconds + " ms");

            int res_read = 0;
            foreach (var i in check_reader)
                res_read += i.Count;

            int res_wrote = 0;
            foreach (var i in check_writer)
                res_wrote += i.Count;

            Console.WriteLine("\nChecking: \nRead\t{0}\nWrite\t{1}", res_read, res_wrote);
        }

        static void Lock_Read()
        {
            obj = new object();
            check_reader = new List<List<string>>();
            List<string> temp = new List<string>();

            while (!finish)
            {
                if (!bEmpty)
                {
                    lock (obj)
                    {
                        if (!bEmpty)
                        {
                            temp.Add(buffer);
                            bEmpty = true;
                        }
                    }
                }
            }

            check_reader.Add(temp);

            Console.WriteLine("Reader " + Thread.CurrentThread.ManagedThreadId + " reads " + temp.Count + " records.");
        }
        static void Lock_Write()
        {
            obj = new object();
            check_writer = new List<List<string>>();
            List<string> temp = Enumerable.Range(0, N).Select(
                n => n.ToString() + " " + Thread.CurrentThread.ManagedThreadId
                ).ToList();

            int i = 0;
            while (i < N)
            {
                if (bEmpty)
                {
                    lock (obj)
                    {
                        if (bEmpty)
                        {
                            buffer = temp[i++];
                            bEmpty = false;
                        }
                    }
                }
            }

            check_writer.Add(temp);

            Console.WriteLine("Writer " + Thread.CurrentThread.ManagedThreadId + " wrote " + i + " records.");
        }
        static void Task2_lock()
        {
            Readers = new Thread[group];
            Writers = new Thread[group];

            sw = new Stopwatch();

            finish = false;
            bEmpty = true;

            sw.Start();
            for (int i = 0; i < group; i++)
            {
                Writers[i] = new Thread(Lock_Write);
                Writers[i].Start();
            }
            for (int i = 0; i < group; i++)
            {
                Readers[i] = new Thread(Lock_Read);
                Readers[i].Start();
            }
            for (int i = 0; i < group; i++)
            {
                Writers[i].Join();
            }

            finish = true;

            for (int i = 0; i < group; i++)
            {
                Readers[i].Join();
            }
            sw.Stop();

            TimeSpan ts = sw.Elapsed;
            Console.WriteLine("Task 2:\tUsing lock: " + ts.TotalMilliseconds + " ms");

            int res_read = 0;
            foreach (var i in check_reader)
                res_read += i.Count;

            int res_wrote = 0;
            foreach (var i in check_writer)
                res_wrote += i.Count;

            Console.WriteLine("\nChecking: \nRead\t{0}\nWrite\t{1}", res_read, res_wrote);
        }

        static void AutoResetEvent_Read(object obj)
        {
            var evFull = ((object[])obj)[0] as AutoResetEvent;
            var evEmpty = ((object[])obj)[1] as AutoResetEvent;

            check_reader = new List<List<string>>();
            List<string> temp = new List<string>();

            while (true)
            {
                evFull.WaitOne();

                if (finish)
                {
                    evFull.Set();
                    check_reader.Add(temp);
                    Console.WriteLine("Reader " + Thread.CurrentThread.ManagedThreadId + " reads " + temp.Count + " records.");
                    break;
                }

                temp.Add(buffer);
                evEmpty.Set();
            }
        }
        static void AutoResetEvent_Write(object obj)
        {
            var evFull = ((object[])obj)[0] as AutoResetEvent;
            var evEmpty = ((object[])obj)[1] as AutoResetEvent;

            check_writer = new List<List<string>>();
            List<string> temp = Enumerable.Range(0, N).Select(
                n => n.ToString() + " " + Thread.CurrentThread.ManagedThreadId
                ).ToList();

            int i = 0;
            while (i < N)
            {
                evEmpty.WaitOne();
                buffer = temp[i++];
                evFull.Set();
            }

            check_writer.Add(temp);

            Console.WriteLine("Writer " + Thread.CurrentThread.ManagedThreadId + " wrote " + i + " records.");
        }
        static void Task2_AutoResetEvent()
        {
            Readers = new Thread[group];
            Writers = new Thread[group];

            sw = new Stopwatch();

            finish = false;

            sw.Start();
            AutoResetEvent[] events = new AutoResetEvent[2]
            {
                new AutoResetEvent(false),
                new AutoResetEvent(true)
            };

            obj = events;

            for (int i = 0; i < group; i++)
            {
                Writers[i] = new Thread(AutoResetEvent_Write);
                Writers[i].Start(obj);
            }
            for (int i = 0; i < group; i++)
            {
                Readers[i] = new Thread(AutoResetEvent_Read);
                Readers[i].Start(obj);
            }
            for (int i = 0; i < group; i++)
            {
                Writers[i].Join();
            }

            finish = true;
            events[0].Set();

            for (int i = 0; i < group; i++)
            {
                Readers[i].Join();
            }
            sw.Stop();

            TimeSpan ts = sw.Elapsed;
            Console.WriteLine("Task 2:\tUsing AutoResetEvent: " + ts.TotalMilliseconds + " ms");

            int res_read = 0;
            foreach (var i in check_reader)
                res_read += i.Count;

            int res_wrote = 0;
            foreach (var i in check_writer)
                res_wrote += i.Count;

            Console.WriteLine("\nChecking: \nRead\t{0}\nWrite\t{1}", res_read, res_wrote);
        }

        static void Semaphore_Read()
        {
            check_reader = new List<List<string>>();
            List<string> temp = new List<string>();

            while (true)
            {
                read_semaphore.WaitOne();
                if (finish)
                {
                    read_semaphore.Release();
                    check_reader.Add(temp);
                    Console.WriteLine("Reader " + Thread.CurrentThread.ManagedThreadId + " reads " + temp.Count + " records.");
                    break;
                }

                temp.Add(buffer);

                write_semaphore.Release();
            }
        }
        static void Semaphore_Write()
        {
            check_writer = new List<List<string>>();
            List<string> temp = Enumerable.Range(0, N).Select(
                n => n.ToString() + " " + Thread.CurrentThread.ManagedThreadId
                ).ToList();

            int i = 0;
            while (i < N)
            {
                write_semaphore.WaitOne();

                buffer = temp[i++];
                read_semaphore.Release();
            }

            check_writer.Add(temp);

            Console.WriteLine("Writer " + Thread.CurrentThread.ManagedThreadId + " wrote " + i + " records.");
        }
        static void Task2_Semaphore()
        {
            Readers = new Thread[group];
            Writers = new Thread[group];
            sw = new Stopwatch();

            finish = false;

            read_semaphore = new Semaphore(0, group);
            write_semaphore = new Semaphore(1, group);

            sw.Start();
            for (int i = 0; i < group; i++)
            {
                Writers[i] = new Thread(Semaphore_Write);
                Writers[i].Start();
            }
            for (int i = 0; i < group; i++)
            {
                Readers[i] = new Thread(Semaphore_Read);
                Readers[i].Start();
            }
            for (int i = 0; i < group; i++)
            {
                Writers[i].Join();
            }

            finish = true;
            read_semaphore.Release();

            for (int i = 0; i < group; i++)
            {
                Readers[i].Join();
            }
            sw.Stop();

            TimeSpan ts = sw.Elapsed;
            Console.WriteLine("Task 2:\tUsing Semaphore: " + ts.TotalMilliseconds + " ms");

            int res_read = 0;
            foreach (var i in check_reader)
                res_read += i.Count;

            int res_wrote = 0;
            foreach (var i in check_writer)
                res_wrote += i.Count;

            Console.WriteLine("\nChecking: \nRead\t{0}\nWrite\t{1}", res_read, res_wrote);
        }

        static void Interlocked_Read()
        {
            check_reader = new List<List<string>>();
            List<string> temp = new List<string>();

            while (!finish)
            {
                if (1 == Interlocked.CompareExchange(ref can_read, 0, 1))
                {
                    temp.Add(buffer);
                    Interlocked.CompareExchange(ref can_write, 1, 0);
                }
            }

            check_reader.Add(temp);

            Console.WriteLine("Reader " + Thread.CurrentThread.ManagedThreadId + " reads " + temp.Count + " records.");
        }
        static void Interlocked_Write()
        {
            check_writer = new List<List<string>>();
            List<string> temp = Enumerable.Range(0, N).Select(
                n => n.ToString() + " " + Thread.CurrentThread.ManagedThreadId
                ).ToList();

            int i = 0;
            while (i < N)
            {
                if (1 == Interlocked.CompareExchange(ref can_write, 0, 1))
                {
                    buffer = temp[i++];
                    Interlocked.CompareExchange(ref can_read, 1, 0);
                }
            }

            check_writer.Add(temp);

            Console.WriteLine("Writer " + Thread.CurrentThread.ManagedThreadId + " wrote " + i + " records.");
        }
        static void Task2_interlocked()
        {
            Readers = new Thread[group];
            Writers = new Thread[group];
            sw = new Stopwatch();

            finish = false;

            can_read = 0;
            can_write = 1;

            sw.Start();
            for (int i = 0; i < group; i++)
            {
                Writers[i] = new Thread(Interlocked_Write);
                Writers[i].Start();
            }
            for (int i = 0; i < group; i++)
            {
                Readers[i] = new Thread(Interlocked_Read);
                Readers[i].Start();
            }

            for (int i = 0; i < group; i++)
            {
                Writers[i].Join();
            }

            finish = true;

            for (int i = 0; i < group; i++)
            {
                Readers[i].Join();
            }
            sw.Stop();

            TimeSpan ts = sw.Elapsed;
            Console.WriteLine("Task 2:\tUsing Interlocked: " + ts.TotalMilliseconds + " ms");

            int res_read = 0;
            foreach (var i in check_reader)
                res_read += i.Count;

            int res_wrote = 0;
            foreach (var i in check_writer)
                res_wrote += i.Count;

            Console.WriteLine("\nChecking: \nRead\t{0}\nWrite\t{1}", res_read, res_wrote);
        }

        static void Main(string[] args)
        {
            //Task1();

            //Task2_lock();

            //Task2_AutoResetEvent();

            //Task2_Semaphore();

            Task2_interlocked();
        }
    }
}