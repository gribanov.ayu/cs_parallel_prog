﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab2
{
    struct Package_1
    {
        public int Beg { get; set; }
        public int End { get; set; }
    }

    struct Package_2
    {
        public int StartingIndex { get; set; }
        public int Beg { get; set; }
        public int End { get; set; }
    }

    class Program
    {
        static int[] N = { 100, 1000, 10000, 100000 };
        static int[] M = { 2, 4, 6, 8 };
        static bool[] flags;
        static List<int> primes;
        static Stopwatch sw;
        static Thread[] threads;
        static int current_index;

        static void InitArray(int n)
        {
            flags = new bool[n];
            primes = new List<int>();
            sw = new Stopwatch();
        }

        static void Step1()
        {
            int len = (int)Math.Sqrt(flags.Length);

            for (int i = 2; i * i <= len; i++)
            {
                if (flags[i - 2] == false)
                {
                    for (int j = i * i; j <= len; j += i)
                        flags[j - 2] = true;
                }
            }

            for (int i = 2; i <= len; i++)
            {
                if (flags[i - 2] == false)
                    primes.Add(i);
            }

            //foreach (int i in primes)
            //{
            //    Console.WriteLine(i);
            //}

            //foreach (bool i in flags)
            //{
            //    Console.WriteLine(i);
            //}
        }
        static void Task1()
        {
            for (int a = 0; a < N.Length; a++)
            {
                InitArray(N[a]);

                sw.Start();
                Step1();
                // Step2()
                for (int i = (int)(Math.Sqrt(N[a])); i < N[a]; i++)
                {
                    for (int j = 0; j < primes.Count; j++)
                    {
                        if (i % primes[j] == 0)
                        {
                            flags[i] = true;
                        }
                    }
                }

                sw.Stop();
                TimeSpan ts = sw.Elapsed;
                Console.WriteLine("Колличество элементов: " + N[a] + ". Время: " + ts.TotalMilliseconds + " ms");
            }
        }

        static void Task2()
        {
            for (int a = 0; a < N.Length; a++)
            {
                InitArray(N[a]);

                for (int b = 0; b < M.Length; b++)
                {
                    threads = new Thread[M[b]];

                    int from = (int)Math.Sqrt(N[a]);

                    sw.Start();
                    Step1();
                    // Step2()
                    for (int i = 0; i < M[b]; i++)
                    {
                        Package_1 package = new Package_1();

                        package.Beg = from + (N[a] - from) / M[b] * i;
                        package.End = (i == M[b] - 1) ?
                            N[a] :
                            package.Beg + (N[a] - from) / M[b];

                        //Console.WriteLine(package.Beg);
                        //Console.WriteLine(package.End + "\n");

                        threads[i] = new Thread(Task2ThreadFunc);
                        threads[i].Start(package);
                    }

                    for (int i = 0; i < M[b]; i++)
                    {
                        threads[i].Join();
                    }
                    sw.Stop();
                    TimeSpan ts = sw.Elapsed;
                    Console.WriteLine("Колличество элементов: " + N[a] + ". \tПотоков: " + M[b] + ". \tВремя: " + ts.TotalMilliseconds + " ms");
                }
            }
        }
        static void Task2ThreadFunc(object obj)
        {
            Package_1 boarders = (Package_1)obj;

            for (int i = boarders.Beg; i < boarders.End; i++)
            {
                for (int j = 0; j < primes.Count; j++)
                {
                    if (i % primes[j] == 0)
                    {
                        flags[i] = true;
                    }
                }
            }
        }

        static void Task3()
        {
            for (int a = 0; a < N.Length; a++)
            {
                InitArray(N[a]);

                for (int b = 0; b < M.Length; b++)
                {
                    threads = new Thread[M[b]];

                    int from = (int)Math.Sqrt(N[a]);

                    sw.Start();
                    Step1();
                    // Step2()
                    for (int i = 0; i < M[b]; i++)
                    {
                        Package_2 package = new Package_2();

                        package.StartingIndex = (int)Math.Sqrt(N[a]);

                        package.Beg = (primes.Count / M[b]) * i;
                        package.End = (i == M[b] - 1) ?
                            primes.Count :
                            package.Beg + primes.Count / M[b];

                        //Console.WriteLine(package.Beg);
                        //Console.WriteLine(package.End + "\n");

                        threads[i] = new Thread(Task3ThreadFunc);
                        threads[i].Start(package);
                    }

                    for (int i = 0; i < M[b]; i++)
                    {
                        threads[i].Join();
                    }
                    sw.Stop();
                    TimeSpan ts = sw.Elapsed;
                    Console.WriteLine("Колличество элементов: " + N[a] + ". \tПотоков: " + M[b] + ". \tВремя: " + ts.TotalMilliseconds + " ms");
                }
            }
        }
        static void Task3ThreadFunc(object obj)
        {
            Package_2 package = (Package_2)obj;

            for (int i = package.StartingIndex; i < flags.Length; i++)
            {
                for (int j = package.Beg; j < package.End; j++)
                {
                    if (i % primes[j] == 0)
                    {
                        flags[i] = true;
                    }
                }
            }
        }

        static void Task4()
        {
            for (int a = 0; a < N.Length; a++)
            {
                InitArray(N[a]);

                sw.Start();
                Step1();
                // Step2()
                ManualResetEvent[] events = new ManualResetEvent[primes.Count];
                for (int i = 0; i < primes.Count; i++)
                {
                    events[i] = new ManualResetEvent(false);
                    ThreadPool.QueueUserWorkItem(Task4ThreadFunc, new object[] { primes[i], events[i] });
                }

                //WaitHandle.WaitAll(events);

                for (int i = 0; i < events.Length; i++)
                {
                    events[i].WaitOne();
                }
                sw.Stop();
                TimeSpan ts = sw.Elapsed;
                Console.WriteLine("Колличество элементов: " + N[a] + ". \tEvents: " + primes.Count + ". \tВремя: " + ts.TotalMilliseconds + " ms");
            }
        }
        static void Task4ThreadFunc(object obj)
        {
            int prime = (int)((object[])obj)[0];

            ManualResetEvent ev = ((object[])obj)[1] as ManualResetEvent;

            for (int i = (int)Math.Sqrt(flags.Length); i < flags.Length; i++)
            {
                if (i % prime == 0)
                {
                    flags[i] = true;
                }
            }

            ev.Set();
        }

        static void Task5()
        {
            for (int a = 0; a < N.Length; a++)
            {
                InitArray(N[a]);
                for (int b = 0; b < M.Length; b++)
                {
                    threads = new Thread[M[b]];

                    sw.Start();
                    Step1();
                    // Step2()
                    for (int i = 0; i < M[b]; i++)
                    {
                        threads[i] = new Thread(Task5ThreadFunc);
                        threads[i].Start(i);
                    }
                    for (int i = 0; i < M[b]; i++)
                    {
                        threads[i].Join();
                    }

                    sw.Stop();
                    TimeSpan ts = sw.Elapsed;
                    Console.WriteLine("Колличество элементов: " + N[a] + ". \tПотоков: " + M[b] + ". \tВремя: " + ts.TotalMilliseconds + " ms");
                }
            }
        }
        static void Task5ThreadFunc(object obj)
        {
            current_index = 0;
            int current_prime;

            while (true)
            {
				
				lock (obj)
                {
					current_prime = primes[current_index];
					if (current_index >= primes.Count)
						break;
					current_index++;
                }

				for (int i = (int)Math.Sqrt(flags.Length); i < flags.Length; i++)
				{
					if (i % current_prime == 0)
					{
						flags[i] = true;
					}
				}
            }
        }

        static void Main(string[] args)
        {
            //Console.WriteLine("Последовательный алгоритм.");
            //Task1();

            //Console.WriteLine("№1 - Декомпозиция по данным.");
            //Task2();

            //Console.WriteLine("№2 - Декомпозиция набора простых чисел.");
            //Task3();

            //Console.WriteLine("№3 - Применение пула потоков.");
            //Task4();

            Console.WriteLine("№4 - Последовательный перебор простых чисел.");
            Task5();
        }
    }
}